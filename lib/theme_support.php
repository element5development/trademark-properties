<?php

/*----------------------------------------------------------------*\
	EXTERNAL JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.1', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 1.1, true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
	HTML 5 SUPPORT
\*----------------------------------------------------------------*/
add_theme_support('html5', array(
	'caption', 
	'comment-form', 
	'comment-list', 
	'gallery', 
	'search-form'
));

/*----------------------------------------------------------------*\
	ENABLE FEATURED IMAGES
\*----------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );

/*----------------------------------------------------------------*\
	ADD & CUSTOMIZE EXCERPTS
\*----------------------------------------------------------------*/
add_post_type_support( 'page', 'excerpt' );
function get_excerpt($limit, $source = null){
	$excerpt = get_the_excerpt();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'...';
	return $excerpt;
}

/*----------------------------------------------------------------*\
	TESTIMONIALS SORT
\*----------------------------------------------------------------*/
add_action( 'pre_get_posts', 'my_orderby_filter' );
function my_orderby_filter( $query ) {
    if ( 'testimonial' === $query->get( 'post_type' ) ) {
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
    }
}
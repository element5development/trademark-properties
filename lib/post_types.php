<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
// Register Custom Post Type Partner
function create_partner_cpt() {

	$labels = array(
		'name' => _x( 'Partners', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Partner', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Partners', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Partner', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Partner Archives', 'textdomain' ),
		'attributes' => __( 'Partner Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Partner:', 'textdomain' ),
		'all_items' => __( 'All Partners', 'textdomain' ),
		'add_new_item' => __( 'Add New Partner', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Partner', 'textdomain' ),
		'edit_item' => __( 'Edit Partner', 'textdomain' ),
		'update_item' => __( 'Update Partner', 'textdomain' ),
		'view_item' => __( 'View Partner', 'textdomain' ),
		'view_items' => __( 'View Partners', 'textdomain' ),
		'search_items' => __( 'Search Partner', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Partner', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Partner', 'textdomain' ),
		'items_list' => __( 'Partners list', 'textdomain' ),
		'items_list_navigation' => __( 'Partners list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Partners list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Partner', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-users',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'about/partners' ),
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'partner', $args );

}
add_action( 'init', 'create_partner_cpt', 0 );

// Register Custom Post Type Team
function create_team_cpt() {

	$labels = array(
		'name' => _x( 'Team', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Team', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Team', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Team', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Team Archives', 'textdomain' ),
		'attributes' => __( 'Team Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Team:', 'textdomain' ),
		'all_items' => __( 'All Team', 'textdomain' ),
		'add_new_item' => __( 'Add New Team', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Team', 'textdomain' ),
		'edit_item' => __( 'Edit Team', 'textdomain' ),
		'update_item' => __( 'Update Team', 'textdomain' ),
		'view_item' => __( 'View Team', 'textdomain' ),
		'view_items' => __( 'View Team', 'textdomain' ),
		'search_items' => __( 'Search Team', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Team', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Team', 'textdomain' ),
		'items_list' => __( 'Team list', 'textdomain' ),
		'items_list_navigation' => __( 'Team list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Team list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Team', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-admin-users',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'create_team_cpt', 0 );

// Register Custom Post Type Testimonial
function create_testimonial_cpt() {

	$labels = array(
		'name' => _x( 'Testimonials', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Testimonial', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Testimonials', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Testimonial', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Testimonial Archives', 'textdomain' ),
		'attributes' => __( 'Testimonial Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Testimonial:', 'textdomain' ),
		'all_items' => __( 'All Testimonials', 'textdomain' ),
		'add_new_item' => __( 'Add New Testimonial', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Testimonial', 'textdomain' ),
		'edit_item' => __( 'Edit Testimonial', 'textdomain' ),
		'update_item' => __( 'Update Testimonial', 'textdomain' ),
		'view_item' => __( 'View Testimonial', 'textdomain' ),
		'view_items' => __( 'View Testimonials', 'textdomain' ),
		'search_items' => __( 'Search Testimonial', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Testimonial', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'textdomain' ),
		'items_list' => __( 'Testimonials list', 'textdomain' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Testimonials list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Testimonial', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-testimonial',
		'supports' => array('title', 'editor', 'excerpt', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'rewrite' => array( 'slug' => 'testimonials' ),
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'create_testimonial_cpt', 0 );
<?php

/*----------------------------------------------------------------*\
	SHOTCODE
\*----------------------------------------------------------------*/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
    'url' => '#',
		'type' => '',
		'color' => '',
		'size' => '',
  ), $atts ) );
  $link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.' is-'.$color.' is-'.$size.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode('button', 'cta_button');
<?php

/*----------------------------------------------------------------*\
	SPECIFY WHAT BLOCKS ARE ALLOWED
\*----------------------------------------------------------------*/
function e5_allowed_block_types( $allowed_blocks ) {
	return array(
		'core/heading',
		'core/paragraph',
		'core/quote',
		'core/list',
		'core/image',
		'core/gallery',
		'core/cover-image',
		'core/file',
		'core/table',
		'core/freeform',
		'core/html',
		'core/button',
		'core/text-columns',
		'core/media-text',
		'core/separator',
		'core/spacer',
		'core/shortcode',
		'core/embed',
		'core-embed/youtube',
		'core-embed/vimeo',
		'core-embed/wordpress',
	);
}
add_filter( 'allowed_block_types', 'e5_allowed_block_types' );
/*----------------------------------------------------------------*\
	DISABLE BLOCK CUSTOM COLORS AND FONT SIZES
\*----------------------------------------------------------------*/
function gutenberg_disable_custom_options() {
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'disable-custom-font-size' );
}
add_action( 'after_setup_theme', 'gutenberg_disable_custom_options' );
/*----------------------------------------------------------------*\
	SET BLOCK FONT SIZES
\*----------------------------------------------------------------*/
function gutenberg_font_sizes() {
	add_theme_support( 
		'editor-font-sizes', array(
			array(
				'name'      => __( 'small' ),
				'shortName' => __( 'S'),
				'size'      => 12,
				'slug'      => 'small'
			),
			array(
				'name'      => __( 'regular' ),
				'shortName' => __( 'R'),
				'size'      => 16,
				'slug'      => 'regular'
			),
			array(
				'name'      => __( 'medium' ),
				'shortName' => __( 'M'),
				'size'      => 24,
				'slug'      => 'medium'
			),
			array(
				'name'      => __( 'large' ),
				'shortName' => __( 'L'),
				'size'      => 32,
				'slug'      => 'large'
			),
		)
	);
}
add_action( 'after_setup_theme', 'gutenberg_font_sizes' );
/*----------------------------------------------------------------*\
	SET BLOCK COLOR PALETTE
\*----------------------------------------------------------------*/
function gutenberg_color_palette() {
	add_theme_support(
		'editor-color-palette', array(
			array(
				'name'  => esc_html__( 'Green' ),
				'slug' => 'green',
				'color' => '#00674A',
			),
			array(
				'name'  => esc_html__( 'Blue' ),
				'slug' => 'blue',
				'color' => '#3FA5D0',
			),
			array(
				'name'  => esc_html__( 'Yellow' ),
				'slug' => 'yellow',
				'color' => '#FFBE00',
			),
			array(
				'name'  => esc_html__( 'Black' ),
				'slug' => 'black',
				'color' => '#000000',
			),
			array(
				'name'  => esc_html__( 'White' ),
				'slug' => 'white',
				'color' => '#FFFFFF',
			),
		)
	);
}
add_action( 'after_setup_theme', 'gutenberg_color_palette' );

/*----------------------------------------------------------------*\
	DISABLE WP PAGE BUILDER FROM ACF TEMPLATE
\*----------------------------------------------------------------*/
function disable_editor_by_template( $id = false ) {
	$excluded_templates = array(
		'templates/flexible-content.php',
		'templates/confirmation.php'
	);
	if( empty( $id ) )
		return false;
	$id = intval( $id );
	$template = get_page_template_slug( $id );
	return in_array( $template, $excluded_templates );
}
function disable_gutenberg_by_template( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;
	if( disable_editor_by_template( $_GET['post'] ) )
		$can_edit = false;
	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'disable_gutenberg_by_template', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'disable_gutenberg_by_template', 10, 2 );
<?php 
/*----------------------------------------------------------------*\

	HOME/FRONT PAGE TEMPLATE
	Customized home page commonly composed of various reuseable sections.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header-home'); ?>

<main id="main-content">
	<article>
		<?php
			if( have_rows('article') ):
				while ( have_rows('article') ) : the_row();

					if( get_row_layout() == 'basic_editor' ):
						get_template_part('template-parts/sections/wysiwyg');
					elseif( get_row_layout() == 'two_col_editor' ): 
						get_template_part('template-parts/sections/wysiwyg-two');
					elseif( get_row_layout() == 'media_and_text' ): 
						get_template_part('template-parts/sections/media-text');
					elseif( get_row_layout() == 'banner' ): 
						get_template_part('template-parts/sections/banner');
					elseif( get_row_layout() == 'gallery' ): 
						get_template_part('template-parts/sections/gallery');
					elseif( get_row_layout() == 'card_repeater' ): 
						get_template_part('template-parts/sections/card-repeater');
					elseif( get_row_layout() == 'info_card_repeater' ): 
						get_template_part('template-parts/sections/info-card-repeater');
					endif;

				endwhile;
			endif; 
		?>
	</article>
</main>

<?php get_template_part('template-parts/sections/testimonials'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>
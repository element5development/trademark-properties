<?php 
/*----------------------------------------------------------------*\

	DEFAULT SINGLE POST TEMPLATE
	More commonly only used for the default Blog/News post type.
	This is the page template for the post type, for the preview
	look under template-parts.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
//BACKGROUND IMAGE?
if ( has_post_thumbnail() ) :
	$class = 'has-image';
	$background = get_the_post_thumbnail_url(get_the_ID(),'xlarge');
else:
	$class = '';
	$background = '';
endif;
?>

<header class="page-title <?php echo $class; ?>" style="background-image: url('<?php echo $background; ?>');">
	<section>
		<h1><?php the_title(); ?></h1>
	</section>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php if ( comments_open() || get_comments_number() ) : ?>
	<?php comments_template(); ?>
<?php endif; ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>
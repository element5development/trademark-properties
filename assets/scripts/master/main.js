var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
  	LOGIC FOR LOAD MORE BUTTON
	\*----------------------------------------------------------------*/
	if ($('.next.page-numbers').length) {
		$('.load-more').css('display', 'table');
	}
	/*----------------------------------------------------------------*\
		INFINITE SCROLL INIT
	\*----------------------------------------------------------------*/
	$('.feed').infiniteScroll({
		path: '.next.page-numbers',
		append: '.preview',
		button: '.load-more',
		scrollThreshold: false,
		checkLastPage: true,
		status: '.page-load-status',
		history: false,
	});
	/*----------------------------------------------------------------*\
  	RESIZE LOGO ON SCROLL
	\*----------------------------------------------------------------*/
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 300) {
			$(".primary-nav-wrap").addClass("is-minimal");
		} else {
			$(".primary-nav-wrap").removeClass("is-minimal");
		}
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE SEARCH
	\*----------------------------------------------------------------*/
	$('button.activate-search').click(function () {
		$('nav.primary-nav .search-form').addClass('is-active');
		$('nav.primary-nav .search-form form input').focus();
	});
	$(document).click(function (event) {
		//if you click on anything except the search input, search button, or search open button, close the search
		if (!$(event.target).closest("nav.primary-nav .search-form form input,nav.primary-nav .search-form form button,button.activate-search").length) {
			$('nav.primary-nav .search-form').removeClass('is-active');
		}
	});
	/*----------------------------------------------------------------*\
  	ACTIVATE MENU
	\*----------------------------------------------------------------*/
	$("#activate-menu").click(function () {
		$(".menu-items-container").addClass("is-active");
	});
	$("#close-menu").click(function () {
		$(".menu-items-container").removeClass("is-active");
	});
	/*----------------------------------------------------------------*\
  	TESTIMONIAL SLIDER
	\*----------------------------------------------------------------*/
	$('.testimonial-slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<svg width="33" height="25" viewBox="0 0 33 25" xmlns="http://www.w3.org/2000/svg"><path d="M11.868 1.001l-10 9a2.995 2.995 0 0 0-.993 2.23c0 .884.384 1.68.993 2.23l10 9a3 3 0 0 0 4.014-4.459l-4.189-3.772h18.182a3 3 0 1 0 0-6H11.693l4.19-3.77a3 3 0 1 0-4.014-4.459" fill="#00A576" fill-rule="evenodd"/></svg>',
		nextArrow: '<svg width="32" height="25" viewBox="0 0 32 25" xmlns="http://www.w3.org/2000/svg"><path d="M21.083 1.001l10 9c.61.55.993 1.345.993 2.23 0 .884-.384 1.68-.993 2.23l-10 9a3 3 0 0 1-4.014-4.459l4.19-3.772H3.075a3 3 0 1 1 0-6h18.182L17.07 5.46a3 3 0 1 1 4.014-4.459" fill="#00A576" fill-rule="evenodd"/></svg>',
	});
	/*----------------------------------------------------------------*\
  	MAKE ABOUT ACTIVE FOR SINGLE PARTNERS
	\*----------------------------------------------------------------*/
	if ($('body.single-partner').length) {
		$("#menu-primary-left-1 .menu-about").addClass("active");
	}

});
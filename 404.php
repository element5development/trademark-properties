<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="page-title">
	<section>
		<h1>404</h1>
		<h2>Page Not Found</h2>
		<p>We can't seem to find the page you're looking for.</p>
		<a href="/" class="button is-large">Home</a>
	</section>
</header>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>
<?php 
/*----------------------------------------------------------------*\

	Template Name: Resources
	resources page template
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main id="main-content">
	<article>
		<?php the_content(); ?>

		<?php get_template_part('template-parts/sections/resource-repeater'); ?>

		<?php get_template_part('template-parts/sections/content-repeater'); ?>

		<?php get_template_part('template-parts/sections/link-repeater'); ?>

	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>
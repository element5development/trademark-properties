<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<header class="page-title">
	<section>
		<?php if ( get_field('page_title') ) : ?>
			<h1><?php the_field('page_title'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>
		<p><?php the_field('title_description'); ?></p>
	</section>
</header>

<main id="main-content">
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>
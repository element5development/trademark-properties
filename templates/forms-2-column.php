<?php 
/*----------------------------------------------------------------*\

	Template Name: 2 Column Forms
	custom page template and design to display a single form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>
	<article class="half-container">
		<section>
			<?php the_field('content'); ?>
		</section>
		<section>
			<?php the_field('form'); ?>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>
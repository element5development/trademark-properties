<?php 
/*----------------------------------------------------------------*\

	TEAM SECTION

\*----------------------------------------------------------------*/
?>

<?php // Custom WP query team
	$args_team = array(
		'post_type' => array('team'),
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
	);
	$team = new WP_Query( $args_team );
?>

<?php if ( $team->have_posts() ) : ?>
	<section class="team-feed">
		<h3>About our team</h3>
		<p>We surround ourselves with people who share our passion for service and success. We are proud of our team.</p>
		<div class="team">
			<?php while ( $team->have_posts() ) : $team->the_post(); ?>
				<article>
					<?php $image = get_field('headshot'); ?>
					<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
					<h4><?php the_title(); ?></h4>
					<p><?php the_field('job_title'); ?></p>
					<p class="description"><?php the_field('short_description'); ?></p>
				</article>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; wp_reset_postdata(); ?>
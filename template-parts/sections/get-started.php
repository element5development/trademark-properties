<?php 
/*----------------------------------------------------------------*\

	GET STARTED SECTION AT END OF SERVICE PAGES

\*----------------------------------------------------------------*/
?>

<?php if ( get_field('title') ) : ?>
<section class="get-started">
	<div>
	<?php if ( get_field('title') ) : ?>
		<h3><?php the_field('title'); ?></h3>
	<?php endif; ?>
	<?php if ( get_field('content') ) : ?>
		<?php the_field('content'); ?>
	<?php endif; ?>
	<?php if( have_rows('button_repeater') ): ?>
		<div>
	 <?php while ( have_rows('button_repeater') ) : the_row(); ?>

	 	<?php $link = get_sub_field('button'); ?>
		<?php if( $link ): ?>
			<a class="button has-icon" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>

		<?php endwhile; ?>
		</div>
	<?php endif; ?>
	</div>
</section>
<?php endif; ?>
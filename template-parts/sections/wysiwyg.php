<?php 
/*----------------------------------------------------------------*\

	STANDARD WYSIWYG

\*----------------------------------------------------------------*/
?>

<section class="wysiwyg-block has-normal-width">
	<div>
		<?php the_sub_field('wysiwyg'); ?>
	</div>
</section>
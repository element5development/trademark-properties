<?php 
/*----------------------------------------------------------------*\

	RESOURCES REPEATER SECTION

\*----------------------------------------------------------------*/
?>
<?php if( get_field('resource_repeater') ): ?>
<section class="resource-repeater">
	<h4>Helpful Information for Homeowners</h4>
	<div>
		<?php while ( have_rows('resource_repeater') ) : the_row(); ?>
			<?php $link = get_sub_field('resource'); ?>
			<?php if( $link ): ?>
			<a class="resource" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
			<?php if( get_sub_field('thumbnail') ): ?>
				<?php $image = get_sub_field('thumbnail'); ?>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php else : ?>
				<svg viewBox="0 0 149 157" xmlns="http://www.w3.org/2000/svg">
					<g fill="none" fill-rule="evenodd">
						<path fill="#DEEBF0" d="M.25.688h148v156H.25z"/>
						<path d="M78.85 83.688c.8.6 1.8 1.3 3.1 2.1 1.5-.2 2.9-.3 4.3-.3 3.6 0 5.8.6 6.5 1.8.4.5.4 1.2.1 1.9v.1l-.1.1c-.1.9-1 1.4-2.6 1.4-1.2 0-2.6-.2-4.2-.7-1.6-.5-3.2-1.1-4.8-2-5.4.6-10.3 1.6-14.5 3.1-3.8 6.4-6.7 9.7-8.9 9.7-.4 0-.7-.1-1-.3l-.9-.4-.2-.2c-.2-.2-.3-.7-.2-1.3.2-1 .9-2.1 2.1-3.4 1.2-1.3 2.8-2.5 4.9-3.6.3-.2.6-.1.8.2l.1.1c1.3-2.1 2.6-4.5 3.9-7.3 1.7-3.3 3-6.6 3.8-9.7-.6-2-1-4-1.1-5.9-.2-1.9-.1-3.5.2-4.7.3-1 .8-1.5 1.5-1.5h.8c.6 0 1 .2 1.3.6.4.5.5 1.2.4 2.5v1.8c0 3-.4 5.4-.7 7.1 1.4 4 3.2 6.9 5.4 8.8zm-21.2 15.1c1.3-.6 3-2.5 5.1-5.8-1.3 1-2.3 2-3.2 3.1-1 1.1-1.6 2-1.9 2.7zm10.1-9.5c3.3-1.3 6.8-2.3 10.5-3 0 0-.2-.1-.5-.4-.3-.2-.5-.4-.6-.5-1.9-1.6-3.4-3.8-4.7-6.5-.7 2.1-1.7 4.5-3.1 7.3-.7 1.4-1.2 2.4-1.6 3.1zm4.6-24.4c-.4 1-.4 2.7-.1 4.9 0-.2.1-.7.3-1.6 0-.1.1-.6.3-1.6 0-.1-.1-1.5-.4-1.8-.1 0-.1 0-.1.1zm19.2 23.8c-.6-.6-2.3-.9-5.2-.9 1.9.7 3.4 1 4.6 1h.7s0-.1-.1-.1zm-4.7-51.4h-47.3v79.8h69.5v-58.1l-22.2-21.7zm-1.4 5.6l17.3 17h-17.3v-17zm-40.9 69.2v-69.8h35.8v22.7h23.6v47.2h-59.4v-.1z" fill="#3894BA" fill-rule="nonzero"/>
					</g>
				</svg>
			<?php endif; ?>
				<?php echo $link['title']; ?>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>
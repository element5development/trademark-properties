<?php 
/*----------------------------------------------------------------*\

	CONTENT REPEATER SECTION

\*----------------------------------------------------------------*/
?>
<?php if( get_field('content_repeater') ): ?>
<section class="content-repeater">
	<div>
		<?php while ( have_rows('content_repeater') ) : the_row(); ?>
			<article class="preview preview-post">
				<figure>
					<button href="<?php the_sub_field('video_link'); ?>?modestbranding=1&autoplay=0&fs=1&showinfo=0&rel=0" data-featherlight="iframe" data-featherlight-iframe-width="640" data-featherlight-iframe-height="480" data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media" data-featherlight-iframe-allowfullscreen="true">
						<?php $image = get_sub_field('video_thumbnail'); ?>
						<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
					</button>
				</figure>
				<div>
					<h5><?php the_sub_field('title'); ?></h5>
					<p><?php the_sub_field('description'); ?></p>
				</div>
			</article>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>
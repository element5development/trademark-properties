<?php 
/*----------------------------------------------------------------*\

	TESTIMONIAL SLIDER SECTION

\*----------------------------------------------------------------*/
?>

<?php $posts = get_field('testimonials'); ?>
<?php if( $posts ): ?>
	<section class="testimonials">
		<h2>Homeowner Stories</h2>
		<div class="testimonial-slider">
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<article>
					<?php setup_postdata($post); ?>

					<?php $image = get_field('author_image'); ?>
					<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />

					<h4><?php the_field('author'); ?></h4>
					<p><?php the_field('short_testimonial'); ?></p>
					<a class="button has-icon" href="<?php echo get_site_url(); ?>/testimonials/#post-<?php the_ID(); ?>">
						<svg width="25" height="25" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg">
							<path d="M16.175 13.773l-6.073 3.807-.005-.008a1.185 1.185 0 0 1-.136.073l-.005.002c-.046.02-.093.038-.142.053l-.01.003a.915.915 0 0 1-.064.016l-.011.003c-.024.005-.049.01-.073.013l-.012.001a1.145 1.145 0 0 1-.156.011 1.145 1.145 0 0 1-1.145-1.145V8.968a1.145 1.145 0 0 1 1.79-.945l6.031 3.769a1.144 1.144 0 0 1 .01 1.98m-3.633-10.91c-5.48 0-9.924 4.442-9.924 9.923 0 5.48 4.444 9.924 9.924 9.924 5.481 0 9.924-4.443 9.924-9.924 0-5.48-4.443-9.924-9.924-9.924m0 22.138C5.796 25 .327 19.531.327 12.785.327 6.04 5.796.571 12.541.571c6.746 0 12.214 5.469 12.214 12.214C24.755 19.531 19.287 25 12.541 25" fill="#FFF" fill-rule="evenodd"/>
						</svg>
						View the Story
					</a>
				</article>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		</div>
	</section>
<?php endif; ?>
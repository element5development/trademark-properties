<?php 
/*----------------------------------------------------------------*\

	SIMPLE CARD SECTION

\*----------------------------------------------------------------*/
?>

<?php if( get_sub_field('card_repeater') ): ?>
<section class="cards">
	<div>
		<?php while ( have_rows('card_repeater') ) : the_row(); ?>
			<article>
				<?php $link = get_sub_field('button'); ?>
				<?php if( $link ): ?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"></a>
				<?php endif; ?>

				<h4><?php the_sub_field('title'); ?></h4>

				<?php if( $link ): ?>
				<div class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></div>
				<?php endif; ?>

			</article>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>
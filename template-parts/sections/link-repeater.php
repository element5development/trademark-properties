<?php 
/*----------------------------------------------------------------*\

	LINK REPEATER SECTION

\*----------------------------------------------------------------*/
?>
<?php if( get_field('link_repeater') ): ?>
<section class="link-repeater">
	<h4>Other Helpful Links</h4>
	<div>
		<?php while ( have_rows('link_repeater') ) : the_row(); ?>
			<?php $link = get_sub_field('link'); ?>
			<?php if( $link ): ?>
			<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>
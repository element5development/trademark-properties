<?php 
/*----------------------------------------------------------------*\

	MEDIA + TEXT

\*----------------------------------------------------------------*/
?>


<section class="media-text has-large-width media-<?php the_sub_field('position'); ?>">
	<figure>
		<?php $image = get_sub_field('image'); ?>
		<img src="<?php echo$image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
	</figure>
	<div>
		<?php the_sub_field('wysiwyg'); ?>
	</div>
</section>
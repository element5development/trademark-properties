<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>
<?php 
	$phone_link = get_field('primary_phone_number','options');
	$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
	$text_link = get_field('text_phone_number','options');
	$text_link = preg_replace('/[^0-9]/', '', $text_link);
?>
<footer>
	<div>
		<div class="who-we-are">
			<h5>Who we are</h5>
			<?php the_field('who_we_are', 'options'); ?>
		</div>
		<div class="contact-us">
			<h5>Contact us</h5>
			<p><?php the_field('business_name', 'options'); ?><br>
			<?php the_field('address', 'options'); ?><br>
			<?php the_field('city', 'options'); ?>, <?php the_field('state', 'options'); ?> <?php the_field('zip_code', 'options'); ?>
			</p>
			<p>Phone: <a href="tel:+1<?php echo $phone_link; ?>"><?php the_field('primary_phone_number', 'options') ?></a><br>
			Text: <a href="sms:<?php echo $text_link; ?>"><?php the_field('text_phone_number', 'options') ?></a><br>
			Fax: <?php the_field('fax_number', 'options') ?>
			</p>
			<p>©Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?></p>
		</div>
		<div class="footer-menu">
			<?php wp_nav_menu(array( 'theme_location' => 'footer_one_nav' )); ?>	
			<?php wp_nav_menu(array( 'theme_location' => 'footer_two_nav' )); ?>
		</div>
	</div>
	<div id="element5-credit" data-emergence="hidden">
		<a target="_blank" href="https://element5digital.com">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
	<div class="social-links">
		<h5>Connect with us</h5>
		<a href="<?php the_field('linkedin', 'options'); ?>" target="_blank">
			<svg>
				<use xlink:href="#linkedin" />
			</svg>
		</a>
		<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
			<svg>
				<use xlink:href="#facebook" />
			</svg>
		</a>
		<a href="<?php the_field('twitter', 'options'); ?>" target="_blank">
			<svg>
				<use xlink:href="#twitter" />
			</svg>
		</a>
		<a href="<?php the_field('youtube', 'options'); ?>" target="_blank">
			<svg>
				<use xlink:href="#youtube" />
			</svg>
		</a>
	</div>
</footer>
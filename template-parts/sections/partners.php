<?php 
/*----------------------------------------------------------------*\

	PARTNERS SECTION

\*----------------------------------------------------------------*/
?>

<?php // Custom WP query partners
	$args_partners = array(
		'post_type' => array('partner'),
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC',
	);
	$partners = new WP_Query( $args_partners );
?>

<?php if ( $partners->have_posts() ) : ?>
	<section class="partners-feed">
		<h3>Meet the Partners</h3>
		<div class="partners">
			<?php while ( $partners->have_posts() ) : $partners->the_post(); ?>
				<article>
					<a href="<?php the_permalink(); ?>"></a>
					<?php $image = get_field('headshot'); ?>
					<div class="image-wrap">
						<a href="<?php the_permalink(); ?>"></a>
						<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						<div class="overlay">
							<a href="<?php the_permalink(); ?>"></a>
							<a class="button" href="<?php the_permalink(); ?>" >View Bio</a>
						</div>
					</div>
					<h4><?php the_title(); ?></h4>
					<p><?php the_field('job_title'); ?></p>

					<?php $link = get_field('linkedin'); ?>
					<?php if( $link ): ?>
					<a class="linkedin" href="<?php echo $link; ?>" target="_blank">
						<svg>
							<use xlink:href="#linkedin" />
						</svg>
					</a>
					<?php endif; ?>

				</article>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; wp_reset_postdata(); ?>
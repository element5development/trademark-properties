<?php 
/*----------------------------------------------------------------*\

	DEFAULT HEADER
	Most basic and simple page title

\*----------------------------------------------------------------*/
?>

<header class="page-title">
	<section>
		<h1><?php the_title(); ?></h1>
		<div class="post-meta">
			<div>
				<?php 
					$get_author_id = get_the_author_meta('ID');
					$get_author_gravatar = get_avatar_url($get_author_id, array('size' => 450)); 
				?>
				<img src="<?php echo $get_author_gravatar; ?>" alt="author avatar" />
			</div>
			<div>
				<p><?php the_author_meta( 'display_name' ); ?> | <?php echo get_the_date('M d, Y'); ?></p>
				<p>			
					<?php 
						$categories = get_the_category($post->ID); 
						$catList = array();
						foreach($categories as $category) {
							$catList[] = $category->name;
						} 
						echo implode(', ', $catList);
					?>
				</p>
			</div>
		</div>
	</section>
</header>
<?php 
	//FEATURED IMAGE?
	if ( has_post_thumbnail() ) :
		$background = get_the_post_thumbnail_url(get_the_ID(),'xlarge');
		$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
	endif;
?>
<figure>
	<img src="<?php echo $background; ?>" alt="<?php echo $alt; ?>" />
</figure>
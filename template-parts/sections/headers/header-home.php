<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>

<?php 
//BACKGROUND IMAGE?
if ( has_post_thumbnail() ) :
	$class = 'has-image';
	$background = get_the_post_thumbnail_url(get_the_ID(),'xlarge');
else:
	$class = '';
	$background = '';
endif;
?>

<header class="page-title <?php echo $class; ?>" style="background-image: url('<?php echo $background; ?>');">
	<section>
		<h2><?php the_field('title'); ?></h2>
		<?php the_field('title_description'); ?>
	</section>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>
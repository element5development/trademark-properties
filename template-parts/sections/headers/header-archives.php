<?php 
/*----------------------------------------------------------------*\

	HEADER FOR ALL ARCHIVES
	Used for any/all post types

\*----------------------------------------------------------------*/
?>

<?php 
	//GET POST TYPE FROM PAGE
	$posttype = get_query_var('post_type');
	//SET DEFAULT POST TYPE
	if ( $posttype == '' ) {
		$posttype = 'blog';
	}
	//BACKGROUND IMAGE?
	if ( get_field( $posttype . '_title_bg_img', 'option') ) :
		$class = 'has-image';
		$background = get_field( $posttype . '_title_bg_img', 'option');
	else:
		$class = '';
		$background = '';
	endif;
?>

<header class="page-title <?php echo $class; ?>" style="background-image: url('<?php echo $background;['sizes']['xlarge'] ?>');">
	<section>
		<h1><?php the_field( $posttype . '_page_title', 'option'); ?></h1>
	</section>
	<?php if ( get_field( $posttype . '_title_bg_img', 'option') ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>
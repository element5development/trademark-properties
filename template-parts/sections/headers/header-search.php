<?php 
/*----------------------------------------------------------------*\

	HEADER FOR SEARCH RESULTS

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image">
	<section>
		<h1>You searched for '<?php echo get_search_query(); ?>'</h1>
	</section>
</header>
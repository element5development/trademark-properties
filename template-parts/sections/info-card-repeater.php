<?php 
/*----------------------------------------------------------------*\

	INFO CARD SECTION

\*----------------------------------------------------------------*/
?>

<?php if( get_sub_field('info_card_repeater') ): ?>
<section class="info-cards">
	<div>
		<?php while ( have_rows('info_card_repeater') ) : the_row(); ?>
			<article>
				<?php $image = get_sub_field('image'); ?>
				<?php if( $image ): ?>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
				<?php endif; ?>

				<?php the_sub_field('content'); ?>

				<?php $link = get_sub_field('button'); ?>
				<?php if( $link ): ?>
				<a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
				<?php endif; ?>
			</article>
		<?php endwhile; ?>
	</div>
</section>
<?php endif; ?>
<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR TESTIMONIALS

\*----------------------------------------------------------------*/
?>

<article class="preview preview-post">
	<a id="post-<?php the_ID(); ?>" class="anchor"></a>
	<figure>
		<button href="<?php the_field('video_link'); ?>?modestbranding=1&autoplay=0&fs=1&showinfo=0&rel=0" data-featherlight="iframe" data-featherlight-iframe-width="640" data-featherlight-iframe-height="480" data-featherlight-iframe-frameborder="0" data-featherlight-iframe-allow="autoplay; encrypted-media" data-featherlight-iframe-allowfullscreen="true">
			<?php $image = get_field('video_thumbnail'); ?>
			<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
		</button>
	</figure>
	<div>
		<h4><?php the_title(); ?></h4>
		<p><?php the_field('testimonial'); ?></p>
		<?php if( get_field('author') ): ?>
			<p><strong>— <?php the_field('author'); ?></strong></p>
		<?php endif; ?>
	</div>
</article>

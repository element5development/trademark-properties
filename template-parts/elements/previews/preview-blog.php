<?php 
/*----------------------------------------------------------------*\

	PREVIEW ELEMENT FOR BLOG/NEWS POSTS

\*----------------------------------------------------------------*/
?>

<?php 
//BACKGROUND IMAGE?
if ( has_post_thumbnail() ) :
	$background = get_the_post_thumbnail_url(get_the_ID(),'small');
	$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);
endif;
?>

<article class="preview preview-post">
	<a href="<?php the_permalink(); ?>"></a>
	<figure>
		<img src="<?php echo $background; ?>" alt="<?php echo $alt; ?>" />
	</figure>
	<h4><?php the_title(); ?></h4>
	<p><?php echo get_excerpt(250); ?></p>
	<div class="buttons">
		<div class="button">Read More</div>
	</div>
</article>

<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>
<?php 
	$phone_link = get_field('primary_phone_number','options');
	$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
?>
<nav class="primary-nav-mobile">
	<a href="tel:+1<?php echo $phone_link; ?>">
		<svg>
			<use xlink:href="#phone" />
		</svg>
	</a>
	<a href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Trademark-Logo.svg" alt="Trademark Properties Logo" />
	</a>
	<button id="activate-menu">
		<svg>
			<use xlink:href="#menu" />
		</svg>
	</button>
	<div class="menu-items-container">
		<button id="close-menu">
			<svg>
				<use xlink:href="#closed" />
			</svg>
		</button>
		<div class="search-form">
			<?php echo get_search_form(); ?>
		</div>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_left_nav' )); ?>	
		<?php wp_nav_menu(array( 'theme_location' => 'primary_right_nav' )); ?>
		<div class="social-links">
			<a href="<?php the_field('linkedin', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
			<a href="<?php the_field('facebook', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#facebook" />
				</svg>
			</a>
			<a href="<?php the_field('twitter', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#twitter" />
				</svg>
			</a>
			<a href="<?php the_field('youtube', 'options'); ?>" target="_blank">
				<svg>
					<use xlink:href="#youtube" />
				</svg>
			</a>
		</div>
	</div>
</nav>
<div class="primary-nav-wrap">
	<nav class="primary-nav">
		<div>
			<button class="activate-search">
				<svg>
					<use xlink:href="#search" />
				</svg>
			</button>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_left_nav' )); ?>
		</div>
		<div class="search-form">
			<?php echo get_search_form(); ?>
		</div>
		<a href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Trademark-Logo.svg" alt="Trademark Properties Logo" />
		</a>
		<div>
			<?php wp_nav_menu(array( 'theme_location' => 'primary_right_nav' )); ?>
		</div>
	</nav>
</div>
<?php 
/*----------------------------------------------------------------*\

	SVG ICONS
	List of all the svg icons used throughout the site.
	Allows for easy access without repeat code.

\*----------------------------------------------------------------*/
?>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
	<symbol id="menu" viewBox="0 0 20 17">
		<g stroke-width="3" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
			<path d="M1.822 2.035h16M1.822 8.272h16M1.822 14.509h16"/>
		</g>
	</symbol>
	<symbol id="closed" viewBox="0 0 16 15">
		<g stroke-width="3" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
			<path d="M2.347 1.887L13.66 13.201M2.63 13.049L13.942 1.735"/>
		</g>
	</symbol>
	<symbol id="search" viewBox="0 0 43 42">
		<defs><path id="a" d="M12.896 12.5H29.42v16.523H12.896z"/></defs>
		<g fill="none" fill-rule="evenodd">
			<circle fill="#F0F0F0" cx="21.532" cy="20.636" r="20.636"/>
			<mask id="b" fill="#fff">
				<use xlink:href="#a"/>
			</mask>
			<path d="M19.093 14.049a4.647 4.647 0 1 0 0 9.294 4.647 4.647 0 0 0 0-9.294zm9.873 12.33a1.55 1.55 0 0 1-2.19 2.19l-4.54-4.54a6.157 6.157 0 0 1-3.143.863 6.196 6.196 0 1 1 6.196-6.196c0 1.149-.318 2.22-.863 3.143l4.54 4.54z" fill="#00A576" mask="url(#b)"/>
		</g>
	</symbol>
	<symbol id="arrowhead-right" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M26 20.006L40 32 26 44.006" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-left" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="4" d="M40 44.006L26 32.012l14-12.006" stroke-linejoin="round"
		  stroke-linecap="round"></path>
	</symbol>
	<symbol id="arrowhead-down" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-miterlimit="10" stroke-width="2" d="M20 26l11.994 14L44 26" stroke-linejoin="round" stroke-linecap="round"></path>
	</symbol>
	<symbol id="linkedin" viewBox="0 0 64 64">
		<path data-name="layer1" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M3.078 22.331h12.188v36.844H3.078z"/>
		<path data-name="layer2" d="M46.719 21.112c-5.344 0-8.531 1.969-11.906 6.281v-5.062H22.625v36.844h12.281V39.206c0-4.219 2.156-8.344 7.031-8.344s7.781 4.125 7.781 8.25v20.063H62V38.269c0-14.532-9.844-17.157-15.281-17.157z" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
		<path data-name="layer1" d="M9.219 4.425C5.188 4.425 2 7.331 2 10.894s3.188 6.469 7.219 6.469 7.219-2.906 7.219-6.469-3.188-6.469-7.219-6.469z" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
	</symbol>
	<symbol id="facebook" viewBox="0 0 64 64">
		<path data-name="layer1" d="M39.312 13.437H47V2h-9.094C26.938 2.469 24.688 8.656 24.5 15.125v5.719H17V32h7.5v30h11.25V32h9.281l1.781-11.156H35.75v-3.469a3.714 3.714 0 0 1 3.562-3.938z" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
	</symbol>
	<symbol id="twitter" viewBox="0 0 64 64">
		<path data-name="layer1" d="M60.448 15.109a24.276 24.276 0 0 1-3.288.968.5.5 0 0 1-.451-.853 15.146 15.146 0 0 0 3.119-4.263.5.5 0 0 0-.677-.662 18.6 18.6 0 0 1-6.527 2.071 12.92 12.92 0 0 0-9-3.75A12.363 12.363 0 0 0 31.25 20.994a12.727 12.727 0 0 0 .281 2.719c-9.048-.274-19.61-4.647-25.781-12.249a.5.5 0 0 0-.83.073 12.475 12.475 0 0 0 2.956 14.79.5.5 0 0 1-.344.887 7.749 7.749 0 0 1-3.1-.8.5.5 0 0 0-.725.477 11.653 11.653 0 0 0 7.979 10.567.5.5 0 0 1-.09.964 12.567 12.567 0 0 1-2.834 0 .506.506 0 0 0-.536.635c.849 3.282 5.092 7.125 9.839 7.652a.5.5 0 0 1 .267.87 20.943 20.943 0 0 1-14 4.577.5.5 0 0 0-.255.942 37.29 37.29 0 0 0 17.33 4.266 34.5 34.5 0 0 0 34.687-36.182v-.469a21.11 21.11 0 0 0 4.934-4.839.5.5 0 0 0-.58-.765z" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2"/>
	</symbol>
	<symbol id="youtube" viewBox="0 0 29 20">
		<path d="M12.74 14.588l-.055-8.842 6.153 4.46-6.097 4.382zM24.273.478L15.107.47 5.582.477C3.199.477.524 2.067.524 4.4v11.61c0 2.332 2.675 3.754 5.058 3.754H24.272c2.383 0 4.314-1.422 4.314-3.754V4.4c0-2.332-1.931-3.923-4.314-3.923z" fill-rule="evenodd"/>
	</symbol>
	<symbol id="email" viewBox="0 0 64 64">
		<path data-name="layer2" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12l30 27.4L62 12" stroke-linejoin="round" stroke-linecap="round"/>
		<path data-name="layer1" fill="none" stroke-miterlimit="10" stroke-width="2" d="M2 12h60v40H2z" stroke-linejoin="round" stroke-linecap="round"/>
	</symbol>
	<symbol id="phone" viewBox="0 0 14 17">
		<path d="M3.538 5.724c.266-.72 1.33-.72 2.022-1.234.213-.154.426-.36.426-.669-.106-1.028-.479-2.724-.851-3.393-.107-.205-.267-.36-.586-.36-.958-.154-2.661.617-2.927.977-4.205 4.936 3.353 16.606 9.793 15.167.426-.103 1.916-1.285 2.236-2.16.053-.257 0-.514-.107-.668-.479-.617-1.916-1.696-2.82-2.21-.267-.155-.533-.052-.8.05-.798.412-1.223 1.338-2.022 1.235-1.49-.206-4.843-5.347-4.364-6.735z" fill-rule="evenodd"/>
	</symbol>
</svg>
<?php 
/*----------------------------------------------------------------*\

	SINGLE PARTNER TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php 
//BACKGROUND IMAGE?
if ( has_post_thumbnail() ) :
	$class = 'has-image';
	$background = get_the_post_thumbnail_url(get_the_ID(),'xlarge');
else:
	$class = '';
	$background = '';
endif;
?>

<?php
$title = get_the_title();
$title_array = explode(' ', $title);
$first_word = $title_array[0];
?>

<header class="page-title <?php echo $class; ?>" style="background-image: url('<?php echo $background; ?>');">
	<section>
		<h1>Meet <?php echo $first_word; ?></h1>
	</section>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="overlay"></div>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php the_content(); ?>

		<div class="contact-links">
		<?php $email = get_field('email'); ?>
			<?php if( $email ): ?>
			<a class="button" href="mailto:<?php echo $email; ?>">Email <?php echo $first_word; ?></a>
			<?php endif; ?>

			<?php 
				$phone_link = get_field('phone_number');
				$phone_link = preg_replace('/[^0-9]/', '', $phone_link);
			?>
			<?php if( $phone_link ): ?>
			<a class="button" href="tel:+1<?php echo $phone_link; ?>">Call <?php echo $first_word; ?></a>
			<?php endif; ?>

			<?php $link = get_field('linkedin'); ?>
			<?php if( $link ): ?>
			<a class="linkedin" href="<?php echo $link; ?>" target="_blank">
				<svg>
					<use xlink:href="#linkedin" />
				</svg>
			</a>
			<?php endif; ?>
		</div>

	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>